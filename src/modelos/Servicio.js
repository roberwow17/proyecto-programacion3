const mongoose = require('mongoose');
const { Schema } = mongoose;

const ServicioSchema = new Schema({
  nombre: String,
  descripcion: String,
  precioPorHora: Number,
  tarea: String
});

mongoose.model('servicios', ServicioSchema);