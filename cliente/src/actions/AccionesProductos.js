import axios from 'axios';
import { tiposProductos } from './types';

export const buscarProductos = () => async dispatch => {
  dispatch({ type: tiposProductos.BUSCAR_PRODUCTOS_PENDIENTE });

  try {
    var res = await axios.get('/api/productos');
    dispatch({ type: tiposProductos.BUSCAR_PRODUCTOS_TERMINADO, payload: res });
  } catch (error) {
    dispatch({ type: tiposProductos.BUSCAR_PRODUCTOS_RECHAZADO, payload: error });
  }
};

export const nuevoProducto = () => async dispatch => {
  dispatch({ type: tiposProductos.NUEVO_PRODUCTO });
};

export const guardarProducto = producto => async dispatch => {
  var res = await axios.post('/api/productos', producto);
  dispatch({ type: tiposProductos.GUARDAR_PRODUCTO, payload: res });
};

export const buscarProductoPorId = id => async dispatch => {
  dispatch({ type: tiposProductos.BUSCAR_PRODUCTOS_POR_ID_PENDIENTE });

  try {
    const res = await axios.get('/api/productos/' + id);
    dispatch({
      type: tiposProductos.BUSCAR_PRODUCTOS_POR_ID_TERMINADO,
      payload: res
    });
  } catch (error) {
    dispatch({ type: tiposProductos.BUSCAR_PRODUCTOS_POR_ID_RECHAZADO });
  }
};

export const actualizarProducto = producto => async dispatch => {
  var res = await axios.put(`/api/productos/${producto._id}`, producto);
  return dispatch({
    type: tiposProductos.ACTUALIZAR_PRODUCTO,
    payload: res
  });
};

export const eliminarProductos = id => async dispatch => {
  var res = await axios.delete(`/api/productos/${id}`);

  return dispatch({
    type: tiposProductos.ELIMINAR_PRODUCTO,
    payload: res
  });
};
