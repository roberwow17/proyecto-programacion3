import { tiposProductos } from '../actions/types';

const ESTADO_INICIAL = {
  listaProductos: [],
  producto: {},
  cargando: false,
  errores: {}
};

export default function(state = ESTADO_INICIAL, action) {
  switch (action.type) {
    case tiposProductos.BUSCAR_PRODUCTOS_TERMINADO:
      return {
        ...state,
        listaProductos: action.payload.data,
        cargando: false
      };

    case tiposProductos.BUSCAR_PRODUCTOS_PENDIENTE:
      return {
        ...state,
        cargando: true
      };

    case tiposProductos.BUSCAR_PRODUCTOS_RECHAZADO:
      return {
        ...state,
        cargando: false
      };

    // case todosTypes.NEW_TODO : {
    //   return {
    //     ...state
    //   }
    // }

    // case todosTypes.SAVE_TODO : {
    //   return {
    //     ...state,
    //     listTodos: [...state.listTodos, action.payload.data],
    //     errors: {}
    //   }
    // }

    case tiposProductos.BUSCAR_PRODUCTOS_POR_ID_TERMINADO: {
      return {
        ...state,
        producto: action.payload.data,
        cargando: false
      };
    }

    case tiposProductos.BUSCAR_PRODUCTOS_POR_ID_PENDIENTE: {
      return {
        ...state,
        producto: {},
        cargando: true
      };
    }

    case tiposProductos.BUSCAR_PRODUCTOS_POR_ID_RECHAZADO: {
      return {
        ...state,
        cargando: false
      };
    }

    // case todosTypes.UPDATE_TODO: {
    //   const todo = action.payload.data;
    //   return {
    //     ...state,
    //     todo: TODO_INITIAL_STATE,
    //     listTodos: state.listTodos.map(item => item._id === todo._id ? todo : item)
    //   }
    // }

    // case todosTypes.DELETE_TODO: {
    //   const id = action.payload.data._id;
    //   return {
    //     ...state,
    //     listTodos: state.listTodos.filter(item => item._id !== id)
    //   }
    // }

    default:
      return state;
  }
}
